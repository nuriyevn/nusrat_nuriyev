import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task_A10
{
//    А10.
//      Дано натуальное число. Найдите сумму и произведение цифр этого числа.
//    Пример входных данных: 456
//    Пример выходных данных: 15 120
    public static void main(String[] args) throws Exception
    {
        // создаем объект принимающий ввод с клавиатуры
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sNum = reader.readLine(); //принимаем с клавиатуры число, которое хранится как строка
        int num = Integer.parseInt(sNum); //парсим строковое число в целое число

        // делаем проверку чтоб число было не менее 10
        while(num < 0)
        {
            System.out.println("Введенное число меньше 0");
            System.out.println("Введите число еще раз");
            sNum = reader.readLine();
            num = Integer.parseInt(sNum);
        }

        // инициализация переменных, хранящих сумму и множество
        int p = 1; // для хранения множества
        int s = 0; // для хранения суммы
        int temp = 0; // для манипуляции промежуточным результатом в цикле

        // цикл разбивающий число
        for ( ; num != 0; )
        {
            temp = num%10;
            num = num / 10;
            p = p * temp;
            s = s + temp;
            
        }
        // вывод результата на экран
        System.out.println("Введенное число " + sNum);
        System.out.println("Сумма его символов " + s);
        System.out.println("Множество его символов " + p);

    }
}
